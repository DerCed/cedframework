<?php
/**
 * Copyright (c) 2016. CedFramework
 * @Author: Cedric R. <theced2012@gmail.com>
 */

session_start();
header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE HTML>
<html lang="de-DE">
<head>
    <title>Salt Generator » CedFramework</title>

</head>
<body style="text-align: center">
<?php

// Thanks for the Code, Brad Christie!
// http://stackoverflow.com/questions/5438760/generate-random-5-characters-string

function salt(){

    $seed = str_split('abcdefghijklmnopqrstuvwxyz'
        .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        .'0123456789'
        .'!@#$%^&*()');
    shuffle($seed);
    $rand = '';
    foreach (array_rand($seed, 20) as $k) $rand .= $seed[$k];

    echo $rand;
}
?>
<h1>Salt Generator</h1>
<p>Or random String Generator... you can call it whatever you want :)</p>
<br>
<hr>
<p>Your random String:</p>
<?php salt(); ?>
<hr>
</body>
</html>
