<?php
/**
 * Copyright (c) 2016. CedFramework
 * @Author: Cedric R. <theced2012@gmail.com>
 */

// registration messages

function email_registration($username, $token)
{

    $username = htmlspecialchars($username, ENT_QUOTES);
    $conf = require "cedframe.php";

    $message = '
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="de">
<head>
<title>Registration message for ' . $conf["website_name"] . '</title>
</head>
<body>
<div style="text-align: center">
<h1>Registration for ' . $conf["website_name"] . '</h1>
<br> <br>
<h4>Hello ' . $username . ',<br> to complete your registration, click please on the button down below!</h4>
<br>
<a href="http://' . $conf["website_domain"] . '/login.php?token=' . $token . '" title="Activate your Email"><button>Complete registration</button></a>
<br><br>
<footer>(C) ' . $conf["website_name"] . ' » Build with CedFrame</footer>
</div>
</body>
</html>
';

    $headline = "Registration for " . $conf["website_name"];

    return array(
        'message' => $message,
        'headline' => $headline
    );
}

?>
