# CedFramework #

This is my basic PHP framework. 
It contains basic MySQL tasks like get information out of a database and other functions witch might get useful while developing.

## Setup ##

* Clone or download this repository
* Configure the Framework 
* Import the CedFramework in your scripts

### Configuration ###

The configuration files are located in the conf/ folder


CedFrame.php

* **website_name** » Specifies the Name of the Website
* **website_domain** » Specifies the Website's Domain (www.example.org)
* **data_format** » Specifies the date Format (for options see [http://php.net/manual/de/function.date.php](Link URL))
* **enable_email_verification** » Sends a email for verification to the new registered user. **This feature works only if email Server is installed!**
* **min_pw_length** » Specifies the minimum length of a password on register
* **upload_allowed_extensions** » Specifies the file types, allowed to be uploaded to the Server through the upload() function (For list of all file types visit [https://www.sitepoint.com/web-foundations/mime-types-complete-list/](Link URL))
* **upload_max_file_size** » Specifies the maximum file upload (in bytes)


email.php

* **message** » Specifies the message of the email, send for the registration
* **headline** » The heading for the Email


mysql.php

* **mysql_host** » Specifies the MySQL server's address (e.g. localhost)
* **mysql_username** » Specifies the MySQL username (*Pro Tip:* Create a new user with restricted permissions for your web project to prevent eventual Hacking)
* **mysql_password** » Specifies the MySQL password 
* **mysql_database** » Specifies the MySQL database


salt.php

* **salt** » Specifies the Salt. The salt is used for extra protection for the password encryption. Please use a random computer generated String for the best security! A generator is located in tools/salt_gen.php but you can use any other String generator too.

### Functions ###

**General**

All of the functions give back a array of information


```
#!php

echo $array['status'];
```
gives back, if the function was successful (ok) or not (error).



```
#!php

echo $array['data'];
```
gives the requested information back, when status is ok or gives back the error message when the status is error


**MySQL functions**



```
#!php

$mysqli = mysqli_connect_function();
```
Gives back a connection to the Mysql Server




```
#!php

$mysqli_return = mysqli_get($select_in_table, $mysql_table, $mysql_where [optional], $mysql_where_string [needed when $mysql_where is defined]);
```
Makes a MySQL query and fetches it.
You can use it to get information out of a MySQL database

* $select_in_table » What information you want to have (e.g. '*' for all)
* $mysql_table » Defines on witch table you want the information of
* $mysql_where » Defines the WHERE tag in a MySQL query (row) (e.g. 'id' for row id)
* $mysql_where_string » Defines the search value of the WHERE tag



**Credential functions**


```
#!php

$login_return = login($post_email, $post_password)
```
Login function.
Returns a 'ok' Status when the user is logged in correctly

* $post_email » The Email, from the email field
* $post_password » The Password, from the password field



```
#!php

$register_return = register($post)
```
Register function.
Returns a 'ok' status when the user registered successfully

* $post » The post from the register field. 
 
HTML Setup:

```
#!HTML
<!-- For email -->
<input type="email" name="email">

<!-- For username -->
<input type="text" name="username">

<!-- For password -->
<input type="password" name="password">
 
```



```
#!php

// EXPERIMENTAL
activate_account($token)
```

* $token » Activation token from the registration email (needed when email registration is enabled)


** Other functions **


```
#!php

$random_return = random_string($length)
```
Gives back a random string.

* $length » Defines the length of the random String (e.g. '5' for 5 char long random string)
### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact