<?php
/**
 * Copyright (c) 2016. CedFramework
 * @Author: Cedric R. <theced2012@gmail.com>
 */

return array(

    ### General ###

    'website_name' => 'CedFrame Website',

    'website_domain' => 'example.com',

    // Date format -- See http://php.net/manual/de/function.date.php for options
    'date_format' => 'd-m-Y',

    ### Credential Management ###

    // Email verification -- When enabled, user will get an registration Email
    // Works only when email server is installed
    'enable_email_verification' => '0',

    //Minimum of Characters witch must be in the Password
    'min_pw_length' => '5',

    ### Upload ###

    // Upload Directory: Where should the files be saved?
    'upload_dir' => '/var/www/uplaods',

    // file types, allowed to upload to the Server -- See https://www.sitepoint.com/web-foundations/mime-types-complete-list/ for options
    'upload_allowed_extensions' => array(

        // Example
        // 'image/gif',
        // 'image/png'
        'image/png',
        'image/jpeg'

    ),

    // Maximal file size, allowed to be uploaded (in bytes)
    // 5 megabytes = 41943040 bytes
    'upload_max_file_size' => '41943040',


);