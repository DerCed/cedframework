<?php
/**
 * Copyright (c) 2016. CedFramework
 * @Author: Cedric R. <theced2012@gmail.com>
 */

return array(


    // Add more protection to your password database by typing in a computer generated string.
    // You can use the Salt Generator, located in the tools folder.

    'salt' => ''

)
?>