<?php
/**
 * Copyright (c) 2016. CedFramework
 * @Author: Cedric R. <theced2012@gmail.com>
 */

function get_cedframe_dir()
{
    return array(

        'cedframe_dir' => '/cedframe/',
        'cedframe_conf_dir' => '/conf/'

    );

}

// Mysql functions
function mysqli_connect_function()
{

    $cedframe_loc = get_cedframe_dir();

    $conf = require($cedframe_loc["cedframe_dir"] . 'mysql.php');

    $mysql = mysqli_connect($conf['mysql_host'], $conf['mysql_username'], $conf['mysql_password'], $conf['mysql_database']);

    if ($mysql == "") {
        return array("status" => "error", "data" => "cannot_connect", "mysql_err_no" => mysqli_connect_errno(), "mysql_err" => mysqli_connect_error());
    }
    mysqli_query($mysql, "SET NAMES 'utf8'");

    return $mysql;
}

function mysqli_get($mysql_select, $mysql_table, $mysql_where, $mysql_where_string)
{

    $mysql = mysqli_connect_function();

    $mysql_select = htmlspecialchars($mysql_select, ENT_QUOTES);
    $mysql_table = htmlspecialchars($mysql_table, ENT_QUOTES);


    if (is_array($mysql) == FALSE) :

        if (isset($mysql_select)):

            if (isset($mysql_table)):

                if (isset($mysql_where) == 1 AND isset($mysql_where_string) == 1):
                    $mysql_where = htmlspecialchars($mysql_where, ENT_QUOTES);
                    $mysql_where_string = htmlspecialchars($mysql_where_string, ENT_QUOTES);
                    $command = "SELECT " . $mysql_select . " FROM " . $mysql_table . " WHERE " . $mysql_where . " = '" . $mysql_where_string . "'";
                else:
                    $command = "SELECT " . $mysql_select . " FROM " . $mysql_table;
                endif;

                $result = mysqli_query($mysql, $command);

                mysqli_close($mysql);

                if (empty($result)):
                    return array(
                        'status' => 'ok',
                        'data' => ""
                    );
                endif;

                $mysql_return = mysqli_fetch_assoc($result);
                return array(
                    'status' => 'ok',
                    'data' => $mysql_return
                );

            else:
                return array(
                    'status' => 'error',
                    'data' => 'no_table'
                );
            endif;

        else:
            return array(
                'status' => 'error',
                'data' => 'no_select'
            );
        endif;

    else:
        log_error("MYSQL CONNECT ERROR: " . $mysql["mysql_err"], 1);
        return array(
            'status' => 'error',
            'data' => 'no_connection'
        );
    endif;

}

function mysqli_insert($mysql_table, $mysql_rows, $mysql_values)
{
    $mysql = mysqli_connect_function();

    $mysql_table = htmlspecialchars($mysql_table, ENT_QUOTES);


    if (is_array($mysql) == FALSE) :


        if (isset($mysql_table)):

            if (isset($mysql_rows)):

                $row = $mysql_rows;
                $values = $mysql_values;

                $command = 'INSERT INTO ' . $mysql_table . ' ( ' . $row . ') VALUES (' . $values . ')';

                debug_console("Insert command", $command);

                $result = mysqli_query($mysql, $command);

                $check_result = check_mysqli_query($result, $mysql);

                mysqli_close($mysql);

                debug_console("Insert; status, uplaod 1", $check_result);

                if ($check_result["status"] == "ok"):
                    return array(
                        'status' => 'ok',
                        'data' => ""
                    );
                else:

                    debug_console("Insert; status,upload", $check_result);
                    return array(
                        'status' => 'error',
                        'data' => 'mysql_error',
                        'specific' => $check_result["specific"]
                    );

                endif;
            else:
                return array(
                    'status' => 'error',
                    'data' => 'no_table'
                );
            endif;
        else:
            return array(
                'status' => 'error',
                'data' => 'no_rows'
            );

        endif;


    else:
        log_error("MYSQL CONNECT ERROR: " . $mysql["mysql_err"], 1);
        return array(
            'status' => 'error',
            'data' => 'no_connection'
        );
    endif;
}

function check_mysqli_query($get_mysqli_query, $mysql)
{

    if (!$get_mysqli_query /*$get_mysqli_query == "FALSE"*/) {

        log_error(mysqli_error($mysql) . " ( " . mysqli_errno($mysql) . " )", 1);
        return array(
            'status' => 'error',
            'data' => mysqli_errno($mysql));
    }
    return array(
        'status' => 'ok',
        'data' => 'valid');
}

// Credential functions

function login($post_email, $post_password)
{
    // The login Script is currently configured that the User needs to type in a email address

    $cedframe_loc = get_cedframe_dir();

    $salt = require ($cedframe_loc["cedframe_conf_dir"]."salt.php");

    $mysqli = mysqli_connect_function();

    $post_password = $post_password . $salt["salt"];

    $passwort = md5($post_password);

    $email = htmlspecialchars($post_email, ENT_QUOTES);

    $befehl = "SELECT * FROM user WHERE email = '" . $email . "' AND password = '" . $passwort . "' AND blocked = 0";
    $result = mysqli_query($mysqli, $befehl) or die(mysqli_error($mysqli));
    $user = mysqli_fetch_assoc($result);

    if ($user ['id'] != '') {

        mysqli_query($mysqli, "UPDATE user SET last_login = '" . time() . "' WHERE id = '" . $user['id'] . "'");

        $_SESSION ['user_id'] = $user ['id'];
        $_SESSION ['username'] = $user ['username'];
        $_SESSION ['email'] = $user ['email'];
        $_SESSION ['rang'] = $user ['rang'];
        $_SESSION ['last_login'] = $user ['last_login'];
        // Add here stuff you might need in your SESSION variable


        setcookie("last_email", htmlspecialchars($email, ENT_QUOTES), time() + 60 * 60 * 24 * 30);

        mysqli_close($mysqli);
        return array(
            'status' => 'ok',
        );


    } else {
        $befehl_spieler = "SELECT blocked FROM user WHERE email = '" . $email . "'";
        $ergebnis = mysqli_query($mysqli, $befehl_spieler);
        $user = mysqli_fetch_assoc($ergebnis);

        mysqli_close($mysqli);

        if ($user ["blocked"] == 1) {
            return array(
                'status' => 'error',
                'data' => 'blocked'
            );
        } else {
            return array(
                'status' => 'error',
                'data' => 'unknown_user'
            );
        }
    }


}

function register($post)
{

    $post_username = $post["username"];
    $post_password = $post["password"];
    $post_email = $post["email"];

    $cedframe_loc = get_cedframe_dir();

    $salt = require($cedframe_loc["cedframe_conf_dir"]."salt.php");
    $conf = require($cedframe_loc["cedframe_dir"]."conf.php");

    $token = random_string("20");

    // Checking if required fields are filled

    if (!isset($post_username)) {
        return array(
            'status' => 'error',
            'data' => 'no_username'
        );
    }

    if (!isset($post_password)) {
        return array(
            'status' => 'error',
            'data' => 'no_password'
        );
    }

    if (!isset($post_email)) {
        return array(
            'status' => 'error',
            'data' => 'no_email'
        );
    }

    // Checking the input from the user about mysql injection, etc.

    if (filter_var($post_email, FILTER_VALIDATE_EMAIL) == false) {
        return array(
            'status' => 'error',
            'data' => 'email_invalid'
        );
    } else {
        $email = $post_email;
    }

    $username = htmlspecialchars($post_username, ENT_QUOTES);

    // check if password has the right length

    if (strlen($post_password) <= $conf["min_pw_length"]) {
        return array(
            'status' => 'error',
            'data' => 'password_length'
        );
    }

    $password = md5($post_password . $salt["salt"]);

    // Check if username and password are not occupied

    $email_response = mysqli_get("*", "user", "email", $email);

    if ($email_response["status"] == "ok" AND $email_response["data"] != "") {
        return array(
            'status' => 'error',
            'data' => 'email_occupied'
        );
    } elseif ($email_response["status"] == "error") {
        log_error("Register error (email_check): " . $email_response["data"], 1);
    }

    $username_response = mysqli_get("*", "user", "username", $username);

    debug_console("User_get", $username_response["data"]["id"]);

    if ($username_response["status"] == "ok" AND $username_response["data"]["id"] != "") {
        return array(
            'status' => 'error',
            'data' => 'username_occupied'
        );
    } elseif ($username_response["status"] == "error") {
        log_error("Register error (username_check): " . $username_response["data"], 1);
    }

    // Insert into mysql Database

    $rows = 'username, email, password, time_registered, last_login, email_valid, token, token_active';

    $time = time();

    if ($conf["enable_email_verification"] == 1) {
        $data = '"' . $username . '", "' . $email . '", "' . $password . '", "' . $time . '", "' . $time . '", "0", "' . $token . '", "1"';

    } else {
        $data = '"' . $username . '", "' . $email . '", "' . $password . '", "' . $time . '", "' . $time . '", "1", "' . $token . '", "0"';

    }


    $response_insert = mysqli_insert("user", $rows, $data);

    debug_console("Insert", $response_insert);

    if ($response_insert["status"] == "error") {
        return array(
            'status' => 'error',
            'data' => $response_insert["data"],

        );
    }

    if ($conf["enable_email_verification"] == 1) {

        $cedframe_loc = get_cedframe_dir();
        require ($cedframe_loc["cedframe_conf_dir"]."email.php");

        $email_content = email_registration($username, $token);

        send_email($email, $email_content["headline"], $email_content["message"], $conf["server_domain"]);

    }

    return array(
        'status' => 'ok'
    );

}

function activate_account($token)
{

    $user_token = mysqli_get('*', 'user', 'token', $token);

    if ($user_token["token_active"] == 1) {

        //TODO update update to framework own update function

        $mysqli = mysqli_connect_function();
        $mysqli_command = "UPDATE user SET email_valid = 1 WHERE user_id = {$user_token["id"]}";
        $mysqli_query = mysqli_query($mysqli, $mysqli_command);

    }
}


// Other functions
function random_string($length)
{

    $seed = str_split('abcdefghijklmnopqrstuvwxyz'
        . 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        . '0123456789'
        . '');
    shuffle($seed);
    $rand = '';
    foreach (array_rand($seed, $length) as $k) $rand .= $seed[$k];

    return $rand;
}

function log_error($message, $imp)
{

    // Error output: [project_dir]/cedframe/cedframe_log.log
    // $imp = 1 -- Critical
    // $imp = 2 -- Information
    // $imp = NULL -- DEBUG

    $cedframe_loc = get_cedframe_dir();

    $cedframe_conf = require($cedframe_loc["cedframe_dir"]."conf.php");

    $pre_err = date($cedframe_conf["date_format"] . " H:i:s") . " @ " . basename($_SERVER["SCRIPT_FILENAME"], '.php') . ".php | ";

    if ($imp == 1):
        //Critical
        echo getcwd() . "/cedframe_log.log";
        $return = error_log($pre_err . "[WARNING] " . $message . "\n", 3, getcwd() . "/cedframe/cedframe_log.log");

    elseif ($imp = 2):
        // Information
        $return = error_log($pre_err . "[INFO] " . $message . "\n", 3, getcwd() . "/cedframe_log.log");

    else:

        // Debug
        $return = error_log($pre_err . "[DEBUG] " . $message . "\n", 3, getcwd() . "/cedframe_log.log");

    endif;

    if ($return == FALSE):

        die("Cannot write into log! Permissions to write?");

    endif;
}

function debug_console($station, $message)
{
    if (is_array($message) || is_object($message)) {
        echo("<script>console.log('[PHPd " . $station . "]: " . json_encode($message) . "');</script>");
    } else {
        echo("<script>console.log('[PHPd " . $station . "]: " . $message . "');</script>");
    }
}

function upload($File)
{

    $cedframe_loc = get_cedframe_dir();

    $cedframe_conf = require($cedframe_loc["cedframe_dir"]."conf.php");

    $upload_dir = $cedframe_conf["upload_dir"];

    $allowed_extensions = $cedframe_conf["upload_allowed_extensions"];

    $max_file_size = $cedframe_conf["upload_max_file_size"];

    $file_error = $File ['userfile'] ['error'];
    if (isset ($File ['userfile'] ['name'])) {
        if ($file_error == '0') {
            $file_type = $File ['userfile'] ['type'];
            if (in_array($file_type, $allowed_extensions) == 1) {
                $file_size = $File ['userfile'] ['size'];
                if ($file_size <= $max_file_size) {
                    $file_time_unix = time();
                    $file_rand_int_1 = rand(1, 9);
                    $file_rand_int_2 = rand(1, 9);
                    $file_rand_int_3 = rand(1, 9);
                    $file_rand_int_4 = rand(1, 9);
                    $file_new_name = $file_time_unix . $file_rand_int_1 . $file_rand_int_2 . $file_rand_int_3 . $file_rand_int_4;
                    $path = $_FILES['userfile']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $file_output_move = move_uploaded_file($File ['userfile'] ['tmp_name'], $upload_dir . "/" . $file_new_name . "." . $ext);
                    if ($file_output_move != '1') {
                        log_error("Cannot copy uploaded file (" . $file_output_move . ")", 1);
                        return "err_upload_move_failed";
                    }
                    return array(
                        'status' => "ok",
                        'data' => $file_new_name . "." . $ext
                    );


                } else {
                    return array(
                        'status' => "error",
                        'data' => "err_upload_file_too_large"
                    );


                }
            } else {
                return array(
                    'status' => "error",
                    'data' => "err_upload_extension_not_allowed"
                );
            }
        } elseif ($file_error == '1') {
            return array(
                'status' => "error",
                'data' => "err_upload_file_too_large_php_config"
            );

        } else {
            log_error("Cannot upload file " . $file_error, 2);
            return array(
                'status' => "error",
                'data' => "err_upload_failed"
            );
        }
    } else {
        return array(
            'status' => "error",
            'data' => "err_uplaod_no_file"
        );
    }

}

function send_email($email_to, $headline, $message, $server_domain)
{

    $header = 'MIME-Version: 1.0' . "\r\n";
    $header .= 'Content-type: text/html; charset="UTF-8";' . "\r\n";

    $header .= 'To: ' . $email_to . ' <' . $email_to . '>' . "\r\n";
    $header .= 'From: ' . $server_domain . ' <' . $server_domain . '>' . "\r\n";

    mail($email_to, $headline, $message, $header);
}

?>